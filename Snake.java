import java.util.Random;
public class Snake{
	public int length;
	public String type;
	public String colour;
	
	Random rand = new Random();
	
	public void eat(){
		int num = rand.nextInt(3);
		if(sleep){
			System.out.println(this.type + " is asleep, they cannot go eat");
		}else if(num == 0){
			System.out.println(this.type + " ate a mouse");
		}else if(num == 1){
			System.out.println(this.type + " ate a bunny");
		}else{
			System.out.println(this.type + " failed to catch it's prey");
		}
	}
	
	boolean sleep = false;
	
	public void sleep(){
		sleep = true;
		if(sleep){
			System.out.println(this.type + " is now asleep");
		}else{
			System.out.println(this.type + " hath awoken!");
		}
	}
}