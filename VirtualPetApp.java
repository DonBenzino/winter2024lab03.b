import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Snake[] pit = new Snake[4];
		for(int i = 0; i<pit.length;i++){
			pit[i] = new Snake();
			System.out.println("What type of snake is it?");
			pit[i].type = reader.nextLine();
			System.out.println("How long is it?");
			pit[i].length = Integer.parseInt(reader.nextLine());
			System.out.println("What is it's colour?");
			pit[i].colour = reader.nextLine();
		}
		
		System.out.println(pit[3].type);
		System.out.println(pit[3].length);
		System.out.println(pit[3].colour);
		
		pit[0].eat();
		pit[0].sleep();
	}
}